  let countWins = 0
  let countLose = 0
  let countDraw = 0
  
  const jokenpo = (userChoice) =>{
    const jkp = ['jo', 'ken', 'po']
    let machineChoice =  jkp[Math.floor(Math.random() * jkp.length)]
    let result = ''

    if( userChoice !== undefined){
      if (userChoice === machineChoice){
        countDraw++
        result = 'EMPATE'
      }
      else if (userChoice === 'jo' && machineChoice === 'ken'){
        countWins++
        result = 'VOCÊ GANHOU'
      }
      else if (userChoice === 'ken' && machineChoice === 'po'){
        countWins++
        result = 'VOCÊ GANHOU'
      }
      else if (userChoice === 'po' && machineChoice === 'jo'){
        countWins++
        result = 'VOCÊ GANHOU'
      }
      else{
        countLose++
        result = 'VOCÊ PERDEU, A MAQUINA GANHOU DESSA VEZ'
      }
    }

    showScore()
    return result
  }

  const showResult = (result) => {
    document.getElementById('res').textContent = result
  }

  const showScore = () => {
    document.getElementById('win').textContent = countWins
    document.getElementById('draw').textContent = countDraw
    document.getElementById('lose').textContent = countLose
  }

  const jkpAnimated = () => { 
    setTimeout(( ) => document.getElementById('res').textContent = 'JO')
    setTimeout( () => document.getElementById('res').textContent = 'KEN', 700)
    setTimeout( () => document.getElementById('res').textContent = 'PÔ', 1400)
}

  const playJoKenPo = () => {
    let joIsSelected = false
    let kenIsSelected = false
    let poIsSelected = false

    const getJo = document.getElementById('jo')
    getJo.addEventListener('click', () =>{
        if(joIsSelected === false){
          joIsSelected = true
          kenIsSelected = false
          poIsSelected = false
          
          getPo.setAttribute('class', 'jkp-box-red')
          getKen.setAttribute('class', 'jkp-box-red')
          getJo.setAttribute('class', 'jkp-box-green')
        }else{
          joIsSelected = false
          getJo.setAttribute('class', 'jkp-box-red')
        }
      },false)
    
    const getKen = document.getElementById('ken')
    getKen.addEventListener('click', () => {
      if(kenIsSelected === false){
        kenIsSelected = true
        joIsSelected = false
        poIsSelected = false

        getPo.setAttribute('class', 'jkp-box-red')
        getJo.setAttribute('class', 'jkp-box-red')
        getKen.setAttribute('class', 'jkp-box-green')
      }else{
        kenIsSelected = false
        getKen.setAttribute('class', 'jkp-box-red')
      }
    })
    
    const getPo = document.getElementById('po')
    getPo.addEventListener('click', () => {
      if (poIsSelected === false){
        poIsSelected = true
        kenIsSelected = false
        joIsSelected = false

        getJo.setAttribute('class','jkp-box-red')
        getKen.setAttribute('class', 'jkp-box-red')
        getPo.setAttribute('class', 'jkp-box-green')
      }else{
        poIsSelected = false
        getPo.setAttribute('class', 'jkp-box-red')
      }
    })

    const btn = document.getElementById('btn-input')
      
    btn.addEventListener('click', () => {
      if(joIsSelected === true){
        jkpAnimated()
        setTimeout( () => showResult(jokenpo('jo')) , 2100)
      }
      if(kenIsSelected === true){
        jkpAnimated()
        setTimeout( () => showResult(jokenpo('ken')), 2100)
      }
      if(poIsSelected === true){
        jkpAnimated()
        setTimeout( () => showResult(jokenpo('po')), 2100)
      }
    })
  }
  
  playJoKenPo()

  